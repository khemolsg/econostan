﻿using System;
using UnityEngine;

public class WorldController : MonoBehaviour {

    // 
    public static WorldController Instance { get; protected set; }

    // Tile Sprites
    public Sprite floorSprite;

    // World and tile data
    public World World { get; protected set; }

    // Use this for initialization
	void Start () {

        if (Instance != null) {
            Debug.LogError("There should never be two world controllers.");
        }
        Instance = this;

        // Create a world with empty tiles
        World = new World();

        // Create a GameObject for each of our tiles, so they show visually
        for (int x = 0; x < World.Width; x++) {
            for (int y = 0; y < World.Height; y++) {
                Tile tile_data = World.GetTileAt(x, y);

                // This creates new gameobjects and adds them to the existing scene
                GameObject tile_go = new GameObject();
                tile_go.name = "Tile_" + x + "_" + y;
                tile_go.transform.position = new Vector3(tile_data.X, tile_data.Y, 0);
                tile_go.transform.SetParent(this.transform, true);

                // Add a sprite renderer, but dont bother 
                // setting a sprite because all the tiles are empty
                tile_go.AddComponent<SpriteRenderer>();

                // Tile change Lambda function
                tile_data.RegisterTileTypeChangedCallback( (tile) => { OnTileTypeChanged(tile, tile_go); } );
            }
        }

        World.RandomizeTiles();
    }
	
	// Update is called once per frame
	void Update () {

	}

    // This function should be called automatically when a tile's type gets changed.
    void OnTileTypeChanged(Tile tile_data, GameObject tile_go) {
        if (tile_data.Type == Tile.TileType.Floor) { 
            tile_go.GetComponent<SpriteRenderer>().sprite = floorSprite;
        }
        else if (tile_data.Type == Tile.TileType.Empty) {
            tile_go.GetComponent<SpriteRenderer>().sprite = null;
        }
        else {
            Debug.LogError("OnTileTypeChange - Unrecongnized tile type.");
        }
    }

    // Gets tile at the unity space coords
    public Tile GetTileAtWorldCoord(Vector3 coord) {
        int x = Mathf.FloorToInt(coord.x);
        int y = Mathf.FloorToInt(coord.y);

        return World.GetTileAt(x, y);
    }
}
