﻿using System.Collections;
using System;
using UnityEngine;

public class Tile {

    // TileType is the base type of the tile, for now there is two types
    public enum TileType { Empty, Floor };
    
    private TileType _type = TileType.Empty;

    public TileType Type {
        get { return _type; }
        set {
            TileType oldType = _type;
            _type = value;
            // Call the callback and let things know we have changed.

            if (cbTileTypeChanged != null && oldType != _type) {
                cbTileTypeChanged(this);
            }
        }
    }

    // For now this is what a building will deconstruct into
    LooseObject looseObject;

    // This is something like a building or statue.
    InstalledObject installedObject;

    World world;

    public int X { get; protected set; }
    public int Y { get; protected set; }

    // The function we callback any time our type changes
    Action<Tile> cbTileTypeChanged;

    //Initializes a new instance of the tile class
    public Tile( World world, int x, int y ) {
        this.world = world;
        this.X = x;
        this.Y = y;
    }
    
    // Register a function to be called back when our tile type changes.
    public void RegisterTileTypeChangedCallback(Action<Tile> callback) {
        cbTileTypeChanged += callback;
    }

    // Unregister a callback
    public void UnregisterTileTypeChangedCallback(Action<Tile> callback) {
        cbTileTypeChanged -= callback;
    }
}
