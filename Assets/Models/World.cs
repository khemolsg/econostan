﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class World {

    // 2d array to hold tile data
    Tile[,] tiles;

    // width of the world
    public int Width { get; protected set; }

    // height of the world
    public int Height { get; protected set; }

    // creates a world with 100 by 100 dimensions and binds the tiles to the worlds locations.
    public World(int width = 100, int height = 100) {
        Width = width;
        Height = height;

        tiles = new Tile[Width, Height];

        for (int x = 0; x < Width; x++) {
            for (int y = 0; y < Height; y++) {
                tiles[x, y] = new Tile(this, x, y);
            }
        }

        Debug.Log("World created with " + (Width * Height) + " tiles."); 
    }

    // Crude randomize of tiles either empty or floor for right now
    public void RandomizeTiles() {
        Debug.Log("RandomizeTiles");
        for (int x = 0; x < Width; x++) {
            for (int y = 0; y < Height; y++) {

                if(Random.Range(0, 2) == 0) {
                    tiles[x, y].Type = Tile.TileType.Empty;
                }
                else {
                    tiles[x, y].Type = Tile.TileType.Floor;
                }
            }
        }
    }


    // returns tile at specific x and y
    public Tile GetTileAt(int x, int y) {
        if ( x > Width || x < 0 || y > Height || y < 0) {
            Debug.LogError("Tile("+x+","+y+") is out of range");
            return null;
        }
        return tiles[x, y];
    }

    

}
