﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BottomMenuScript : MonoBehaviour {
    public GameObject StatePanel, PoliciesPanel, FinancesPanel,
                      RelationsPanel, TradePanel, CharacterPanel, 
                      StockpilePanel, RelgionsPanel, ProvincesPanel;


    //RectTransform UICanvas; 


    private void Start() {
        HideAllPanels();
        ResetAllPos();
    }

    public void StateWindow() {
        HideShowPanel(StatePanel);
    }

    public void PoliciesWindow() {
        HideShowPanel(PoliciesPanel);
    }

    public void FinancesWindow() {
        HideShowPanel(FinancesPanel);
    }

    public void RelationsWindow() {
        HideShowPanel(RelationsPanel);
    }

    public void TradeWindow() {
        HideShowPanel(TradePanel);
    }

    public void CharacterWindow() {
        SceneManager.LoadScene(2);
        //HideShowPanel(CharacterPanel);
    }

    public void StockpileWindow() {
        HideShowPanel(StockpilePanel);
    }

    public void ReligionWindow() {
        HideShowPanel(RelgionsPanel);
    }

    public void ProvincesWindow() {
        HideShowPanel(ProvincesPanel);
    }

    private void HideShowPanel(GameObject obj) {
        if (obj.gameObject.activeSelf == true) {
            obj.gameObject.SetActive(false);
        } else {
            HideAllPanels();
            obj.gameObject.SetActive(true);
        }
    }

    private void HideAllPanels() {
        StatePanel.gameObject.SetActive(false);
        PoliciesPanel.gameObject.SetActive(false);
        FinancesPanel.gameObject.SetActive(false);
        RelationsPanel.gameObject.SetActive(false);
        TradePanel.gameObject.SetActive(false);
        CharacterPanel.gameObject.SetActive(false);
        StockpilePanel.gameObject.SetActive(false);
        RelgionsPanel.gameObject.SetActive(false);
        ProvincesPanel.gameObject.SetActive(false);
    }

    private void ResetAllPos() {
        // Resets all positions of panels to 10, 10
        StatePanel.GetComponent<RectTransform>().SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, 10, StatePanel.GetComponent<RectTransform>().rect.width);
        StatePanel.GetComponent<RectTransform>().SetInsetAndSizeFromParentEdge(RectTransform.Edge.Top, 10, StatePanel.GetComponent<RectTransform>().rect.width);

        PoliciesPanel.GetComponent<RectTransform>().SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, 10, StatePanel.GetComponent<RectTransform>().rect.width);
        PoliciesPanel.GetComponent<RectTransform>().SetInsetAndSizeFromParentEdge(RectTransform.Edge.Top, 10, StatePanel.GetComponent<RectTransform>().rect.width);
        
        FinancesPanel.GetComponent<RectTransform>().SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, 10, StatePanel.GetComponent<RectTransform>().rect.width);
        FinancesPanel.GetComponent<RectTransform>().SetInsetAndSizeFromParentEdge(RectTransform.Edge.Top, 10, StatePanel.GetComponent<RectTransform>().rect.width);
        
        RelationsPanel.GetComponent<RectTransform>().SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, 10, StatePanel.GetComponent<RectTransform>().rect.width);
        RelationsPanel.GetComponent<RectTransform>().SetInsetAndSizeFromParentEdge(RectTransform.Edge.Top, 10, StatePanel.GetComponent<RectTransform>().rect.width);
        
        TradePanel.GetComponent<RectTransform>().SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, 10, StatePanel.GetComponent<RectTransform>().rect.width);
        TradePanel.GetComponent<RectTransform>().SetInsetAndSizeFromParentEdge(RectTransform.Edge.Top, 10, StatePanel.GetComponent<RectTransform>().rect.width);
        
        CharacterPanel.GetComponent<RectTransform>().SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, 10, StatePanel.GetComponent<RectTransform>().rect.width);
        CharacterPanel.GetComponent<RectTransform>().SetInsetAndSizeFromParentEdge(RectTransform.Edge.Top, 10, StatePanel.GetComponent<RectTransform>().rect.width);
        
        StockpilePanel.GetComponent<RectTransform>().SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, 10, StatePanel.GetComponent<RectTransform>().rect.width);
        StockpilePanel.GetComponent<RectTransform>().SetInsetAndSizeFromParentEdge(RectTransform.Edge.Top, 10, StatePanel.GetComponent<RectTransform>().rect.width);
        
        RelgionsPanel.GetComponent<RectTransform>().SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, 10, StatePanel.GetComponent<RectTransform>().rect.width);
        RelgionsPanel.GetComponent<RectTransform>().SetInsetAndSizeFromParentEdge(RectTransform.Edge.Top, 10, StatePanel.GetComponent<RectTransform>().rect.width);
        
        ProvincesPanel.GetComponent<RectTransform>().SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, 10, StatePanel.GetComponent<RectTransform>().rect.width);
        ProvincesPanel.GetComponent<RectTransform>().SetInsetAndSizeFromParentEdge(RectTransform.Edge.Top, 10, StatePanel.GetComponent<RectTransform>().rect.width);
    }

}
