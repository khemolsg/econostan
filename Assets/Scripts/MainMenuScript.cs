﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuScript : MonoBehaviour {

    public GameObject OptionsPanel;

    private void Start() {
        HideAllPanels();
        //ResetAllPos();
    }

    public void PlayGame() {
        SceneManager.LoadScene(1);
    }

    public void LoadGame() {
        SceneManager.LoadScene(0);
    }

    public void Options() {
        HideShowPanel(OptionsPanel);
    }

    public void Credits() {
        // to do; scene change to credits with some .mp4 or .avi playing and music + artwork
    }

    public void QuitGame() {
        Application.Quit();
    }

    private void HideShowPanel(GameObject obj) {
        if (obj.gameObject.activeSelf == true) {
            obj.gameObject.SetActive(false);
        } else {
            HideAllPanels();
            obj.gameObject.SetActive(true);
        }
    }

    private void HideAllPanels()
    {
        OptionsPanel.gameObject.SetActive(false);
    }

    private void ResetAllPos()
    {
        // Resets all positions of panels to 10, 10
        OptionsPanel.GetComponent<RectTransform>().SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, Screen.width * 0.35f, OptionsPanel.GetComponent<RectTransform>().rect.width);
        OptionsPanel.GetComponent<RectTransform>().SetInsetAndSizeFromParentEdge(RectTransform.Edge.Top, Screen.height * 0.35f, OptionsPanel.GetComponent<RectTransform>().rect.height);
    }
}
