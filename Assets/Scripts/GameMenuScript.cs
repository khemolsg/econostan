﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameMenuScript : MonoBehaviour {

    public void ResumeGame() {
        
    }

    public void LoadGame() {
        // to do
    }

    public void SaveGame() {
        // to do
    }

    public void Options() {
        // to do; disables certain elements to show a "new menu" with graphics and sound,
        // maybe keybindings
    }

    public void ExitToMenu() {
        SceneManager.LoadScene(0);
    }

    public void ExitToDesktop() {
        Application.Quit();
    }
}
