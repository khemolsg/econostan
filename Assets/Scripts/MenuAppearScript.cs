﻿ using UnityEngine;
 using System.Collections;

public class MenuAppearScript : MonoBehaviour {
    private CanvasGroup CanvasGroup;

    void Start() {
        CanvasGroup = GetComponent<CanvasGroup>();
        CanvasGroup.interactable = false;
    }

    void Update() {
        if (Input.GetKeyUp(KeyCode.Escape)) {
            if (CanvasGroup.alpha == 1) {
                CanvasGroup.alpha = 0;
                CanvasGroup.interactable = false;
            } else {
                CanvasGroup.alpha = 1;
                CanvasGroup.interactable = true;
            }
        }
    }
}